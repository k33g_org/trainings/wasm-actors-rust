

```bash
wat2wasm add.wat

wasm-objdump -x add.wasm

wasm2wat add.wasm

wasmtime add.wasm --invoke add 1 2
wasmtime add.wasm --invoke sub 1 2

wasmtime add.wasm --invoke mul 2 2

wasmtime add.wasm --invoke div 5 2
```
